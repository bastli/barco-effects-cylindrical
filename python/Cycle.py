import sys
import socket
import time
import math
import numpy as np
from random import random
from matplotlib import colors

def rainbow(n):
    """
    Returns a list of colors sampled at equal intervals over the spectrum.

    Parameters
    ----------
    n : int
        The number of colors to return

    Returns
    -------
    R : (n,3) array
        An of rows of RGB color values

    Notes
    -----
    Converts from HSV coordinates (0, 1, 1) to (1, 1, 1) to RGB. Based on
    the Sage function of the same name.
    """
    R = np.ones((1,n,3))
    R[0,:,0] = np.linspace(0, 1, n, endpoint=False)
    #Note: could iterate and use colorsys.hsv_to_rgb
    return 255*colors.hsv_to_rgb(R).squeeze() 


IP_ADDR = sys.argv[1]
IP_PORT = 1337
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

rgb = np.array([[0] * 3 * 112] * 15)

colrs = rainbow(15)
print(colrs)
dt = 0.125

def clear_framebuffer():
        for i, v in enumerate(rgb):
            rgb[i] = 0


def send_framebuffer():
    for i in range(15):
        data = bytes([i]) + bytes(rgb[i].astype(np.uint8))
        sock.sendto(data, (IP_ADDR, IP_PORT))

def clamp(v):
    if(v<0):
        return 0
    if(v>255):
        return
    else:
        return v

def main():
    while True:
        for a in np.arange(0,0.4,0.01):
            clear_framebuffer()
            for s,v in enumerate(rgb):
                rgb[s] = np.tile(colrs[s]*(a%1.0), 112)
            send_framebuffer()
            time.sleep(dt)
        for a in np.arange(0.49,0.0,-0.01):
            clear_framebuffer()
            for s,v in enumerate(rgb):
                rgb[s] = np.tile(colrs[s]*(a%1.0), 112)
            send_framebuffer()
            time.sleep(dt)
        """
        for i in range(15):
            clear_framebuffer()
            rgb[i] = np.tile(colrs[i], 112)
            send_framebuffer()
            time.sleep(dt)
    """
main()
