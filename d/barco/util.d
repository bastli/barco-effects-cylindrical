module barco.util;
import barco.strip;
import std.typecons;
import std.container;
import std.algorithm;
import std.range;
import std.math;

auto distance(Tuple!(int,int) a){
	enum xd = 70.0;
	enum yd = 1.0;
	return a[0]^^2*xd + a[1]^^2*yd;
}

auto modulo(T)(T value, T m) {
    auto mod = value % m;
    if (value < 0) {
        mod += m;
    }
    return mod;
}

float line_distance(Vector x, Vector p1, Vector p2){
	 return abs((p2.y-p1.y)*x.x - (p2.x-p1.x)*x.y + p2.x*p1.y - p2.y*p1.x)/(sqrt((p2.y-p1.y)^^2 + (p2.x - p1.x)^^2));
}

struct Image(T=float){
	enum w = STRIP_COUNT;
	enum h = LED_COUNT;
	T[h][w] board;
	
	int wrap(int x){
		return (x+10*w)%w;
	}
	
	auto byPixel(){
		return board[].map!((ref a)=>a[]).joiner;
	}
	
	void set(T v){
		byPixel().each!((ref p)=>p=v);
	}
	
	auto neighbourhood_map(Range)(uint i, uint j, Range r){
		return r.map!(a=>tuple((i+a[0]+w)%w,(j+a[1]))).filter!(a=>(a[1]>=0 && a[1]<h));
	}
	
	auto neighbourhood(Range)(uint i, uint j, Range r){
		ref get(Tuple!(uint,uint) a){
			return board[a[0]][a[1]];
		}
		return neighbourhood_map(i,j,r).map!get;
	}
	
	enum defaultNeighbourhood = [tuple(-1,-1), tuple(-1,0), tuple(-1,1), tuple(0,-1), tuple(0,1), tuple(1,-1), tuple(1,0), tuple(1,1)];
	
	
	auto neighbourhood(uint i, uint j){
		return neighbourhood(i,j,defaultNeighbourhood);
	}
	
	auto line(Vector a, Vector b){
		int x0 = cast(int)(min(a.x,b.x));
		int x1 = cast(int)(ceil(max(a.x,b.x)));
		int y0 = cast(int)(min(a.y,b.y));
		int y1 = cast(int)(ceil(max(a.y,b.y)));
		return cartesianProduct(iota(x0,x1),iota(y0,y1))
			.filter!(a=>a[1]>=0 && a[1]<h)
			.map!(x=>Vector(wrap(x[0]),x[1]))
			.map!(x=>tuple(x, 1-line_distance(x, a, b)))
			.filter!(a=>a[1]>0)
		;
	}
	
	auto indices(){
		return cartesianProduct(iota(0,w),iota(0,h));
	}
	
	ref opIndex(Vector p){
		return opIndex(cast(size_t)p.x,cast(size_t)p.y);
	}
	ref opIndex(size_t x, size_t y){
		return board[x][y];
	}
}

void blit(in Image!Color img, ref StripArray sa){
	foreach(i,ref s; sa){
		s.set(img.board[i][]);
	}
}

void blit(T)(in Image!T img, ref StripArray sa, Color function(T) cmap){
	foreach(i,ref s; sa){
		s.set(img.board[i][].map!(a=>cmap(a)));
	}
}

struct Vector{
	float x,y;
	Vector opBinary(string op)(in Vector b) const{
		Vector v;
		mixin("v.x = this.x "~op~" b.x;");
		mixin("v.y = this.y "~op~" b.y;");
		return v;
	}
	Vector opBinary(string op)(in float b) const{
		Vector v;
		mixin("v.x = this.x "~op~" b;");
		mixin("v.y = this.y "~op~" b;");
		return v;
	}
	float norm()const{
		return x^^2+y^^2;
	}
}

struct Particle{
	Vector p,v;
	Vector function(Particle) a;
	Color c;
	Color function(Particle) color_transition;
	bool function(Particle) outscoped;
	
	Vector get_a(){
		if(a is null){
			return Vector(0,0);
		}
		return a(this);
	}
	
	bool step(){
		p = p + v;
		v = v + get_a();
		if(!(color_transition is null)){
			c = color_transition(this);
		}
		if(outscoped !is null){
			return !outscoped(this);
		}
		return true;
	}
}

struct Particles{
	SList!Particle system;
	size_t _length=0;
	void opOpAssign(string op="~")(Particle p){
		system.insert(p);
		_length++;
	}
	
	void step(){
		foreach(ref p; system[]){
			if(!p.step()){
				remove(p);
			}
		}
	}
	auto length()const{
		return _length;
	}
	
	void remove(Particle p){
		_length--;
		system.linearRemoveElement(p);
	}
	
	auto particles(){
		return system[];
	}
}

void blit(Particles System, ref StripArray sa){
	foreach(ref s; sa){
		s.set(Color.BLACK.repeat(LED_COUNT));
	}
	foreach(p; System.particles){
		p.p.x = (p.p.x + STRIP_COUNT) % STRIP_COUNT;
		sa.strips[cast(int)p.p.x].leds[cast(int)clamp(p.p.y,0,LED_COUNT-1)].c = convert_to_8bit(p.c);
	}
}


void blit(string op="=")(Particles System, ref Image!Color img){
	foreach(p; System.particles){
		p.p.x = modulo(p.p.x, STRIP_COUNT);
		mixin("img.board[cast(int)p.p.x][cast(int)clamp(p.p.y,0,LED_COUNT-1)] "~op~" p.c;");
	}
}

void blit_smooth(string op="=")(Particles System, ref Image!Color img){
	foreach(p; System.particles){
		p.p.x = (p.p.x + STRIP_COUNT*10) % STRIP_COUNT;
		foreach(pp,w; img.line(p.p,p.p-p.v)){
			mixin("img[cast(int)pp.x,cast(int)pp.y] "~op~" p.c*w;");
		}
	}
}
