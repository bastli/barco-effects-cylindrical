Collection of Barco-Effects
===========================

These effects rely on the barcos being ordered in a cylindrical shape.

Effect Interface
----------------

An effect is an executable that takes an IP on its argv[1], executes an effect for a certain time, and then exits cleanly.

Effect Registry
---------------

Effects are registered by placing them into `runner/effects` with `chmod a+x`.

Running
-------

The effect registry gets run by `cd runner; ./run.sh`
